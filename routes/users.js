
var express = require('express');
var router = express.Router();
var database = require("../data.js")

/* GET home page. */
router.get('/list', function(req, res, next) {
  database.getData("users", {}).then(data=>{
    res.render("skeleton", {user: req.user, content: "users", title:"Express", users:data})
  })
});

router.get('/logout', function(req, res, next) {
  database.getData("users", {}).then(data=>{
    res.render("skeleton", {user: req.user, content: "users", title:"Express", users:data})
  })
});


module.exports = router;
