const express = require("express");
const router = express.Router();

const monodata = require("../data")

router.get("/", (req, res, next) => {
  res.render("skeleton", {user: req.user, content: "forms/register", title:"Express"})
})

router.post("/", (req, res, next) => {
  var re = new RegExp("^[a-z, A-Z][a-zA-Z0-9]{2,11}$");
  if(req.body.username.search(re) != 0)
    res.redirect("/register?error=0");
  monodata.addUser(req.body.username, req.body.password).then(error => {
    console.log(error)
    if(error==0)
      res.redirect("/login")
    else {
      res.redirect("/register?error=1")
    }
  })
})

module.exports = router;
