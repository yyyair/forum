
var express = require('express');
var router = express.Router();
var database = require("../data.js")

//MongoDB stuff
const dbUrl = "mongodb://localhost:27017/forum"
const MongoClient = require("mongodb").MongoClient;
const ObjectID = require("mongodb").ObjectID;

/* GET home page. */
router.get('/', function(req, res, next) {
  /*
  getForumData((categories, forums) => {
    res.render("skeleton", {content: "index", title:"Express", data:{"categories":categories, "forums":forums}});
  })*/

  database.getData("forums", {}).then(query_res => {
    console.log(req.user)
    forums = query_res
    categories = []
    forums.forEach((forum)=>{
      if(!categories.includes(forum.category))
        categories.push(forum.category)
    })
    console.log(categories)
    res.render("skeleton", {user: req.user, content: "index", title:"Express", data:{"categories":categories, "forums":forums}});
  })
});

module.exports = router;
