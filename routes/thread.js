var express = require('express');
var router = express.Router();
const ObjectID = require("mongodb").ObjectID;
const monodata = require("../data")

/* handle thread reading */
router.get('/view', function(req, res, next) {
  var re = new RegExp("^[a-f, 0-9]{24}$");
  if(req.query.id.search(re) != 0)
    res.redirect("/");
  monodata.threadData(req.query.id).then(response => {
    header = response[0][0]
    comments = response[1]
    console.log(comments);
    if(header==undefined)
    {
      res.redirect("/")
    }
    else {
    monodata.viewThread(req.query.id).catch((e)=>{
      console.log(e);
    })
    res.render("skeleton", {user: req.user, "content": "thread", "title": "Express", "comments": comments, "thread":header})
    }
  })
});

/* handle thread creation */

router.get("/new", (req, res, next) => {
  res.render("skeleton", {user: req.user, content: "newThread", title:"Express", forumId: req.query.forumId })
})

router.post("/new", require('connect-ensure-login').ensureLoggedIn(), (req, res, next) => {

  monodata.newThread(req.body.title, req.body.content, req.body.forumId, req.user).then(response=>{
    res.redirect("/forum?id=" + req.body.forumId);
  })

})

router.post("/remove", require('connect-ensure-login').ensureLoggedIn(), (req, res, next) => {
  tId = req.body.thread;
  fId = req.body.f
  monodata.removeThread(tId, fId).then(response=>{
    res.redirect("/forum?id=" + fId);
  })

})


router.post("/comment", (req, res)=>{
  tId = req.body.tId;
  content = req.body.content
  monodata.addComment(tId, content, req.user).then(resp=>{
    res.redirect("/thread/view?id=" + tId);
  })
})
module.exports = router;
