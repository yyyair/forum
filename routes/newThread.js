const express = require("express");
const router = express.Router();

const monodata = require("../data")

router.get("/", (req, res, next) => {
  res.render("skeleton", {user: req.user, content: "newThread", title:"Express", forumId: req.query.forumId })
})

router.post("/", require('connect-ensure-login').ensureLoggedIn(), (req, res, next) => {

  monodata.newThread(req.body.title, req.body.content, req.body.forumId).then(response=>{
    res.redirect("/forum?id=" + req.body.forumId);
  })

})

module.exports = router;
