const express = require("express");
const router = express.Router();
var passport = require('passport');
const monodata = require("../data")


router.get("/", (req, res, next) => {
  res.render("skeleton", {user: req.user, content: "forms/login", title:"Express"})
})


router.post("/", passport.authenticate('local', { failureRedirect: '/login' }), (req, res, next) => {
  res.redirect('/');
})

router.get("/logout", (req, res, next) => {
  req.logout()
  res.redirect('/');
})

module.exports = router;
