var express = require('express');
var router = express.Router();
var mongodata = require("../data.js");
const ObjectID = require("mongodb").ObjectID;
/* GET home page. */
router.get('/', function(req, res, next) {
  var re = new RegExp("^[a-f, 0-9]{24}$");
  if(req.query.id.search(re) != 0)
    res.redirect("/");

  mongodata.forumData(req.query.id).then(response => {
    header = response[0][0]
    threads = response[1]
    data = {"header":header, "threads":threads}
    console.log(data)
    if(header==undefined)
    {
      res.redirect("/")
    } else
    res.render("skeleton", {user: req.user, "content": "forum", "title": "Express", "threads": threads, "header":header})
  })
});

router.get("/edit", (req, res)=>{
  fId = req.query.id
  res.render("skeleton", {user: req.user, "content":"editForum", "title":"Edit", "id":fId})
})

router.post("/edit", (req, res)=>{
  fId = req.body.fId;
  name = req.body.fName;
  manager = req.body.fManager;
  category = req.body.fCategory;

  mongodata.isForumValid(fId, name, manager, category).then(valid=>{
    if(valid==true) {
      mongodata.editForumData(fId, name, category, manager).then(resp=>{
        res.redirect("/?status=yes")
      })

    }
    else {
      res.redirect("/")
    }
  })

})

router.get("/new", (req, res)=>{
  fId = req.query.id
  res.render("skeleton", {user: req.user, "content":"editForum", "title":"Edit", "id":"none"})
})

router.post("/new", (req, res)=>{
  name = req.body.fName;
  manager = req.body.fManager;
  category = req.body.fCategory;

  mongodata.insertData("forums", {"name":name, "category": category, manager:"manager", views: 0}).then(resp=>{
    res.redirect("/")
  })

})

router.post("/remove", (req, res)=>{
  fId = req.body.id

  mongodata.removeData("forums", {"_id": new ObjectID(fId)}).then(resp=>{
    res.redirect("/")
  })

})

module.exports = router;
