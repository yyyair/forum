const MongoClient = require("mongodb").MongoClient;
const dbUrl = "mongodb://localhost:27017/forum"
const ObjectID = require("mongodb").ObjectID;

exports.test = function() {
  console.log("Working");
}

function getData(collection, query) {
  return connect(dbUrl).then(db => {
    return Promise.all([
      getQuery(db, collection, query),
      close(db)
    ])
  }).catch(err => {console.log(err)})
}

function insertData(collection, query) {
  return connect(dbUrl).then(db => {
    return Promise.all([
      insertQuery(db, collection, query),
      close(db)
    ])
  }).catch(err => {console.log(err)})
}

function connect(url) {
  console.log("popop")
  return MongoClient.connect(url)
}

function close(db) {
  return db.close()
}

function getQuery(db, collection, query) {
  console.log("kokok")
  return db.collection(collection)
    .find(query).toArray()
}

function insertQuery(db, collection, query) {
  return db.collection(collection).insert(query)
}



function updateQuery() {

}

getData("users", {}).then(response => {console.log(response[0])})
//forumData("59d60d5fc93161293f5166f1").then(response => {console.log(response[0][0]); console.log(response[1][0])})
//threadData("59d6524ca64a099efa519bc7").then(response => {console.log(response[0][0]); console.log(response[1][0])})

function forumData(forumId) {
  return Promise.all([
    getData("forums", {"_id": new ObjectID(forumId)}),
    getData("threads", {"forumId": forumId})
  ])
}

function threadData(threadId) {
  return Promise.all([
    getData("threads", {"_id": new ObjectID(threadId)}),
    getData("comments", {"threadId": threadId})
  ])
}

function newThread(name, content, forumId) {
  obj = {"title": name, "content": content, "forumId": forumId, sticky: false, views: 0}
  console.log(forumId)
  return insertData("threads", obj)
}

function verifyUser(user, pass) {
  return getData("users", {"username": name, "password":pass})
}

function getUID(name) {
  return getData("users", {"username": name})
}

function getUName(uid) {
  return getData("users", {"_id": new ObjectID(uid)})
}

function addUser(name, pass) {
  return insertData("users", {"username":name, "password":pass})
}

/*exports.getForumContent = getForumContent;
exports.getForumHeader = getForumHeader;
exports.getForumData = getForumData;
*/
exports.getData = getData
exports.insertData = insertData
exports.forumData = forumData
exports.threadData = threadData
exports.newThread = newThread

exports.addUser = addUser
exports.verifyUser = verifyUser
exports.getUID = getUID
exports.getUName = getUName
//exports.isForum = isForum;
