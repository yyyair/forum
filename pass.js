var passport = require('passport');
var Strategy = require('passport-local').Strategy;
const mongodata = require("./data")
var session = require('express-session')({ secret: 'keyboard cat', resave: false, saveUninitialized: false })

passport.use(new Strategy(
  function(username, password, cb) {
    mongodata.verifyUser(username, password).then(r=>{
      cb(null, r[0])
    })
  }));

passport.serializeUser(function(user, cb) {
  cb(null, user._id.toString())
});

passport.deserializeUser(function(id, cb) {
  mongodata.getUName(id).then(res => {
    cb(null, res[0])
  })
});



//exports.pass = passport.initialize()
exports.session = session
//exports.sess = passport.session()
exports._passport = passport
