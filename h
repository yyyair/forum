// Returns a forum header
function getForumHeader(id, callback) {
  MongoClient.connect(dbUrl, (err, db) => {
    if(err == null)
      db.collection("forums").find({_id: id}).toArray((err, arr) => {
        db.close();
        callback(err, arr[0])
      })
    else {
      callback(err, null);
    }
  })
}


// Returns the content of a forum
function getForumContent(id, max, callback) {
  console.log("hei");
  getForumStickyThreads(id, (err1, sthreads) => {
    if(err1==null)
      getForumNormalThreads(id, max, (err2, nthreads) => {
        callback(err2, {"sticky": sthreads, "normal": nthreads});
      })
    else
      callback(err1, "sticky");
  })
}

// Returns an array of threads with sticky = true
function getForumStickyThreads(id, callback) {
  MongoClient.connect(dbUrl, (err, db) => {
    if(err == null)
      db.collection("threads").find({forumId: id, sticky:true}).toArray((err, arr) => {
        db.close();
        callback(err, arr);
      })
    else
      callback(err, null);
  })
}

// Returns an array of threads with sticky = false
function getForumNormalThreads(id, max, callback) {
  MongoClient.connect(dbUrl, (err, db) => {
    if(err == null)
      db.collection("threads").find({forumId: id, sticky:false}).toArray((err, arr) => {
        db.close();
        callback(err, arr);
      })
    else
      callback(err, null);
    })
}

// Returns {header: forumHeader, content:forumContent}
function getForumData(id, maxThreads, callback) {
  getForumHeader(id, (err, header) => {
    if(err == null)
      getForumContent(id.toString(), maxThreads, (err2, content) => {
        callback(err2, {"header": header, "content": content});
      })
    else
      callback(err, null);
  })
}

// Returns false if the given forum id does not exist, otherwise returns the forum header
function isForum(id, callback) {
  getForumHeader(id, (err, header) => {
    if(err)
      callback(err, null);
    else if(header == null)
      callback(err, false);
    else
      callback(err, header);
  })
}

// Executes update command with a given update query, should be a private method hidden to users
function setForumHeader(id, options, callback) {
  MongoClient.connect(dbUrl, (err, db) => {
    db.collection("forums").update({_id: id}, options);
  })
}

// Sets a forum's name
function setForumName(id, name, callback) {
  setForumHeader(id, {$set: {"name":name}}, callback);
}
