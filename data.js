const MongoClient = require("mongodb").MongoClient;
const dbUrl = "mongodb://localhost:27017/forum"
const ObjectID = require("mongodb").ObjectID;


// gets query from collection
function getData(collection, query) {
  return connect(dbUrl).then(db => {
    return Promise.all([
      getQuery(db, collection, query),
      close(db)
    ]).then((res)=>{
      return Promise.resolve(res[0])
    })
  }).catch(err => {console.log(err)})
}

function insertData(collection, query) {
  return connect(dbUrl).then(db => {
    return Promise.all([
      insertQuery(db, collection, query),
      close(db)
    ]).then((res)=>{
      return Promise.resolve(res[0])
    })
  }).catch(err => {console.log(err)})
}

function connect(url) {
  console.log("Connected")
  return MongoClient.connect(url).catch(e=>{console.log("e");})
}

function testConnection(dbUrl) {
  return connect(dbUrl).then(db=>{
    console.log("Works");
    return close(db)
  }).catch(e=>{
    console.log("Error accessing db at " + dbUrl);
  })
}

function close(db) {
  return db.close()
}

function getQuery(db, collection, query) {
  return db.collection(collection)
    .find(query).toArray()
}

function insertQuery(db, collection, query) {
  return db.collection(collection).insert(query)
}



function updateQuery(db, _collection, query, obj) {
  console.log(db + " " + _collection + query + obj);
  return db.collection(_collection).update(query, obj)
}

function removeQuery(db, collection, query) {
  return db.collection(collection).remove(query)
}

function removeData(collection, query) {
  return connect(dbUrl).then((db)=>{
    return Promise.all([
      removeQuery(db, collection, query),
      close(db)])
  })
}

function updateData(collection, query, obj) {
  return connect(dbUrl).then((db)=>{

    return Promise.all([
      Promise.resolve(()=>{console.log(db + "kkk " + collection + query + obj);}),
      updateQuery(db, collection, query, obj),
      close(db)])
  }).catch(e=>{
    console.log(e);
  })
}

getData("comments", {}).then(response => {console.log(response)})
//userExists("uhukk").then(resp => {console.log(resp)})
//forumData("59d60d5fc93161293f5166f1").then(response => {console.log(response[0][0]); console.log(response[1][0])})
//threadData("59d6524ca64a099efa519bc7").then(response => {console.log(response[0][0]); console.log(response[1][0])})

//returns forum header and threads
function forumData(forumId) {
  return Promise.all([
    getData("forums", {"_id": new ObjectID(forumId)}),
    getData("threads", {"forumId": forumId})
  ])
}

function threadData(threadId) {
  return Promise.all([
    getData("threads", {"_id": new ObjectID(threadId)}),
    getData("comments", {"threadId": threadId})
  ])
}

function newThread(name, content, forumId, user) {
  obj = {"title": name, "content": content, "forumId": forumId, sticky: false, views: 0, "creator": user.username}
  console.log(forumId)
  return Promise.all([
    insertData("threads", obj),
    updateData("forums", {"_id":new ObjectID(forumId)}, {$inc:{"threads":1}})
  ])
}

function addComment(tId, c, u) {
  obj = {"content":c, "threadId":tId, "creator":(u==undefined?"Anon":u.username)}
  return insertData("comments", obj)
}

function removeThread(tId, fId) {
  return Promise.all([
    removeData("threads", {"_id": new ObjectID(tId)}),
    updateData("forums", {"_id":new ObjectID(fId)}, {$inc:{"threads":-1}})
  ])
}

function removeForum(fId) {
  return Promise.all([
    removeData("forums", {"_id": new ObjectID(fId)}),
    removeData("threads", {"forumId": fId})
  ])
}

function viewThread(tId) {
  return updateData("threads", {"_id":new ObjectID(tId)}, {$inc:{"views":1}})
}

function isForumValid(id, name, manager, category) {
    console.log(id + " " + name + " " + manager + " " + category + " :::::: ")
    return Promise.all([
      forumData(id),
      getUID(manager)
    ]).then(res=>{
      console.log(res)
      fData = JSON.stringify(res[0][0])
      uId = JSON.stringify(res[1])
      console.log(fData + " : " + uId)
      return Promise.resolve((fData!="[]" && uId != "[]")?true:false)
    })

}

function editForumData(fId, fName, fCat, fManager) {
  return updateData("forums", {_id: new ObjectID(fId)}, {$set:{"name":fName, "category":fCat, "manager":fManager}})
}

/* users */

function verifyUser(user, pass) {
  return getData("users", {"username": user, "password":pass})
}

function getUID(name) {
  return getData("users", {"username": name})
}

function getUName(uid) {
  return getData("users", {"_id": new ObjectID(uid)})
}

function addUser(name, pass) {
  return userExists(name).then(ex=>{
    if(!ex)
      return insertData("users", {"username":name, "password":pass}).then((r)=>{
        return Promise.resolve(0)
      })
    else
      return Promise.resolve(1)
  })
}

function userExists(name) {
  return Promise.resolve(
    getUID(name).then(id=>{
      return id.length>0
    })
  )
}

exports.getData = getData
exports.insertData = insertData
exports.removeData = removeData
exports.forumData = forumData
exports.threadData = threadData
exports.newThread = newThread
exports.removeThread = removeThread
exports.viewThread = viewThread
exports.addComment = addComment
exports.isForumValid = isForumValid
exports.editForumData = editForumData

exports.addUser = addUser
exports.verifyUser = verifyUser
exports.getUID = getUID
exports.getUName = getUName
//exports.isForum = isForum;
